resource "aws_lambda_function" "lambda" {
  runtime                        = var.runtime
  function_name                  = var.function_name
  role                           = element(concat(aws_iam_role.iam.*.arn, [""]), 0)
  handler                        = var.handler
  source_code_hash               = filebase64sha256(var.source_code_zip)
  timeout                        = var.timeout
  memory_size                    = var.memory_size
  reserved_concurrent_executions = var.reserved_concurrent_executions
  description                    = var.description
  tags                           = local.tags
  publish                        = var.publish

  environment {
    variables = local.env_vars
  }

}