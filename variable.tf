variable "runtime" {
  description = "Run time used when calling lambda"
}

variable "enabled" {
  description = "Make it true if you want to create lambda function"
  type        = bool
  default     = true
}

variable "function_name" {
  description = "Lambda function name"
}

variable "handler" {
  description = "Handler name"
}

variable "timeout" {
  description = "Lambda timeout in minutes"
  default = 300
}

variable "memory_size" {
  description = "Lambda memory size"
}

variable "reserved_concurrent_executions" {
  description = "Lambda concurrent execution"
  default =  -1
}

variable "description" {
  description =   "Description of lambda function"
}

variable "publish" {
  description = "Whether to publish creation/change as new Lambda Function Version"
  default     = false
}

variable "source_code_zip" {
  description = "Zip file to be uploaded for Lambda"
}

variable "environment_variables" {
  description = "Key/Value pairs for setting Lambda Environment Variables"
  type        = map(string)
  default     = {}
}

locals {
  env_vars = merge(
  {
    "ENV" = var.tags_eng_app_environment
  },
  var.environment_variables
  )
}

