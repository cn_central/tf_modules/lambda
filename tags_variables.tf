

variable "tags_eng_app_environment" {
  description = "The environment in which the resource resides."
  validation {
    condition     = length(var.tags_eng_app_environment) > 1 && (can(regex("^dev", var.tags_eng_app_environment)) || can(regex("^stage", var.tags_eng_app_environment)) || can(regex("^prod", var.tags_eng_app_environment)))
    error_message = "Please provide valid value environment: 'dev', 'stage', 'prod'?"
  }
}


variable "tags_eng_app_name" {
  description = "The name of the app or general function area of the resource."
}

variable "tags_eng_app_svc" {
  description = "The service provided by the resource or specific function area of the resource."
  default     = "lambda"
}

locals {
  name = "${var.tags_eng_app_environment}-${var.tags_eng_app_name}-${var.tags_eng_app_svc}"

  tags = {
    eng_app_environment   = var.tags_eng_app_environment
    eng_app_name          = var.tags_eng_app_name
    eng_app_svc           = var.tags_eng_app_svc
  }
}